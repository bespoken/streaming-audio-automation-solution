# **Bespoken's Streaming Audio Automation Solution**
This sample project shows how to perform end-to-end testing to a large sets of utterances against Bespoken Virtual Devices. It leverages Bespoken, DataDog, and GitLab CI.

The combination of these tools provides in-depth testing, routine builds, tracking of historical results, and graphical reporting and analytics.

The tests are used to verify audio-stream elements like the streamURL, invocation name, and any other value present in the Response Payload for Amazon Alexa (and possibly Google Assistant in the future).

## **How It Works**
The tester does the following:  
* Reads audio sources from the `source` key value as defined in the `input\configurationFile.json`.
* Turns each utterance into a record
* Calls Alexa with the utterance from the record
* Gets the Stream URL from the response provided by Alexa
* Validates the `streamURL` parameter is the same as the `streamURL` returned by Alexa
* Validates the data from the stream
  * By default, we download ~10 seconds of data
  * We analyze for any period of silence - silence is more than 5 seconds of audio with less -50db volume
  * [More info here](#silence-detection-algorithm)
* Publishes results
  * Results are sent to [DataDog](https://app.datadoghq.com/dashboard/jy5-h96-wuu/online-radio-stations-batch-testing)
  * Results are published as CSV files accessible via [GitLab](https://gitlab.com/bespoken/streaming-audio-automation-solution/-/jobs)

The specialized validation logic described above is performed by the [streaming-audio-interceptor](https://gitlab.com/bespoken/streaming-audio-automation-solution/blob/master/src/streaming-audio-interceptor.js).

## **Configuration**
Our configuration file looks like this:
```json
{
  "silenceDetection": {
    "enabled": false,
    "streamDuration": 10,
    "silenceDuration": 3,
    "level": -50
  },
  "job": "Streaming Audio Automation Solution",
  "metrics.bak": "datadog-metrics",
  "source": "@bespoken-sdk/batch/lib/csv-source",
  "sourceFile": "input/radioList.csv",
  "interceptor": "src/streaming-audio-interceptor",
  "customer": "bespoken",
  "virtualDevices": {
    "<YOUR VIRTUAL DEVICE TOKEN>": ["A name for your token"]
  },
  "limit": 5
}
```

The actual file is [input/configurationFile.json](input/configurationFile.json).

With regard to the virtual devices, they will be used to run tests in parallel. Take note that, we do not recommend using more than three because of the silence detection step, which use FFMPEG.

FFMPEG is very CPU intensive, and running several processes simultaneously is likely to cause errors if it exceeds the CPU available (currently, in GitLab, a single core).

**Limit**
The limit is the maximum number of records to process. 

**Silence Detection Algorithm**
We use three properties to detect silence:

| Property | Description |
| ---  | --- |
| enabled | Whether or not silence detection should be run for the stream
| streamDuration | The duration, in seconds, of audio to download. This is an estimate based on a 54 kbps stream
| silenceDuration | The period of continuous silence that must occur for this to be considered an invalid stream 
| level | The volume level in dB below which the audio is considered silent - set -50 initially

If the stream has silence that exceeds the thresholds for duration and volume, it is marked as invalid.

# **Getting Started**
## **Getting Started With GitLab**
We recommend running tests inside of GitLab. It makes it easy to:
* Run tests in a clean environment
* Run tests on a regular schedule
* Share your tests with other team members

To get started using these tests in GitLab, sign up for a GitLab account, then select 'Fork' on the top-right of this repository, and follow the directions. It will duplicate this project, in your own GitLab environment!

Once forked, you can drop-in your own tests in the `input\radioList.csv` file.

## **Getting Started Locally**
Alternatively, you can run the tests within your own machine (or in a different hosted environment).

Get your local copy of this project by cloning it from this repository.
```bash
git clone https://gitlab.com/bespoken/streaming-audio-automation-solution.git
```

To make it work, you have to install the project dependencies from the project root:
```bash
npm install
```
## **Virtual Device Setup**
We use our Virtual Devices to transform the text test scripts into audio, send them to the voice assistant and process the responses. A virtual device works like a physical device, such as Amazon Echo, but can be interacted with via our [simple test scripting syntax](https://read.bespoken.io/end-to-end/guide.html) (as well as programmatically via our [API](https://read.bespoken.io/end-to-end/api.html)).

- Create a virtual device with our easy-to-follow guide [here](https://read.bespoken.io/end-to-end/setup/#creating-a-virtual-device).
- Add it to the configuration file (`input\configurationFile.json`), in the section shown below:
  ```json
  {
    "virtualDevices": {
      "<YOUR_VIRTUAL_DEVICE>": ["Meta data for your Virtual Device"]
    }
  }
  ```
## **Environment Variables**
There are a couple of environment variables used during test execution:
- `DATADOG_API_KEY`: As we send the test execution results to DataDog for reporting it is necessary to create a DataDog account and then generate an API key to receive the test data.
- `FFMPEG_TOKEN`: The token used to access Bespoken's FFMPEG service. Only used if silence detection is enabled.

To add these in Gitlab, go to `Settings -> CI / CD -> Variables` and add the environment variables, with the appropriatve values, there.

For running locally, the values can be set either in a `.env` file (you can rename the `example.env` and include your values on it); or set them temporarily from the command line with the `export` command:
```bash
export DATADOG_API_KEY=<YOUR-API-KEY>
```
(Use `set` instead of `export` if using Windows Command Prompt).

If you set the DATADOG_API_KEY environment variable, make sure to change the line "metrics.bak" in the [configuration file](input/configurationFile.json) to "metrics".

# **Running Tests**
## **Running Tests In Gitlab**
To manually start a test run in Gitlab, do this: 
* Go to `CI / CD -> Pipelines`
* Click `Run Pipeline`
* Click `Run Pipeline` again

Easy, right?

If using this project as a Fork from Gitlab, the testing are controlled via the file `.gitlab-ci.yml`:
```yaml
image: node:16

cache:
  paths:
  - node_modules/

utterance-tests:
  script:
   - npm install
   - npm run process
  artifacts:
    when: always
    paths:
      - output/results.csv
  only: 
    - schedules
    - web
```
When the GitLab Runner is executed, it takes this file and creates a Linux instance with Node.js, executes the commands under the `script` element, and saves the reports as artifacts.

### **Setting a schedule**
It is very easy to run your end-to-end tests regularly using GitLab. Once your CI file (`.gitlab-ci.yml`) has been uploaded to the repository just go to "CI/CD => Schedules" from the left menu. Create a new schedule, it looks like this:

[<img src="docs/images/GitLabCISchedule-1.png" width="50%">](docs/images/GitLabCISchedule-1.png)

[<img src="docs/images/GitLabCISchedule-2.png" width="50%">](docs/images/GitLabCISchedule-2.png)

## **Running Tests Locally**
Start by making a copy of `example.env` and saving it as `.env`. Set the values with the appropriate credentials and keys. Email support@bespoken.io if you have questions what these should be.

Once installed the dependencies and added your Virtual Device to the `configurationFile.json` file, execute the tests with:
```bash
npm run process
```
When finished, a report is generated under `output`.

# **Test Results**
As mentioned above, results are written to CSV file and DataDog.

In the CSV File, named `output\results.csv`, the following data is written:

| Column | Description |
| --- | --- |
| UTTERANCE | The utterance sent to Alexa |
| ACTUAL STREAMURL | The stream URL returned by the Alexa skill |
| EXPECTED STREAMURL | The stream URL we are expecting to receive so the test passes |
| SUCCESS | The test result |
| STREAM DATA LENGTH | The length of data downloaded and tested successfully |
| SILENCE DURATIONS | A comma-delimited list of silence periods, if any detected in the audio data |
| ERROR | Details of system error, if any, for this test. Usually blank |

# **DataDog**
For DataDog, we add a number of tags to the data that can be used to slice and dice it in a variety of ways. You can see the set of tags [here](https://gitlab.com/bespoken/streaming-audio-automation-solution/blob/master/src/streaming-audio-interceptor.js#L75).

The Dashboard in DataDog looks like this:
<p align='center'>
  <img src='docs/images/DataDogScreenshot.png' width='50%' />
</p>

The metrics can be easily reported on through a DataDog Dashboard. They also can be used to set up notifications when certain conditions are triggered.

## **How to signup and get API key**
[DataDog](https://www.datadoghq.com/) has several plans for different use cases. You can start a 14-day free trial and get your API Key to collect the test results on your dashboard, or use ours, included in our packaged solutions (for more information, [contact us](mailto:sales@bespoken.io)).

After the signup process, you can create your API key following these steps:
1. Select `APIs` from the left `Integrations` menu.
2. Create a new API key in the `API Keys` section.

Then you can copy the value to your `.env` file, or add it as an environment variable on your CI settings.

## **How to export and import a Dashboard**
A DataDog dashboard is a tool for visually tracking, analyzing, and displaying key performance metrics, which enable you to monitor the health of your voice apps. You can create a Dashboard by using the `utterance` and `test` metrics. To help you get started you can import our sample Dashboard by following the next steps:
1. Create a dashboard by selecting `New Dashboard` from the left `Dashboards` menu.
2. Give your dashboard a name, and then click on the `New Timeboard`.
3. Select `Import Dashboard JSON` from the settings cog (upper right corner)
4. Drag [this file](https://gitlab.com/bespoken/streaming-audio-automation-solution/-/blob/master/docs/BespokenDataDogDashboard.json) into the `Import dashboard JSON` modal window
5. Click on `Yes, Replace`

Now, whenever you run your test scripts, the test results will be automatically sent and stored in DataDog.