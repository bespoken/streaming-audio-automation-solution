const _ = require('lodash')
const axios = require('axios')
const Config = require('@bespoken-sdk/shared/lib/config')
const fs = require('fs')
const Interceptor = require('@bespoken-sdk/batch/lib/interceptor')
const Record = require('@bespoken-sdk/batch/lib/record')
const Result = require('@bespoken-sdk/batch/lib/result')

/**
 * @typedef SilenceResult
 * @property {number[]} durations
 * @property {boolean} exceeded
 * @property {string} error
 */

class StreamingAudioInterceptor extends Interceptor {
  constructor () {
    super()
    if (!fs.existsSync('tmp')) {
      fs.mkdirSync('tmp')
    }
    this.checkStreams = Config.get('silenceDetection.enabled', undefined, false, true)
  }

  /**
   * Intercepts results from batch tester and does validation and tagging
   * @param {Record} record
   * @param {Result} result
   */
  async interceptResult (record, result) {
    const customer = Config.get('customer')
    //console.info('result: ' + JSON.stringify(result, null, 2))
    const utterance = _.get(result, 'lastResponse.message')
    const streamURL = _.get(result, 'lastResponse.streamURL')

    // Find the provider
    let alexaProvider
    for (const directive of _.get(result, 'lastResponse.raw.messageBody.directives', [])) {
      alexaProvider = _.get(directive, 'payload.content.provider.name')
      if (alexaProvider) {
        break
      }
    }

    // Set all the columns in our output records
    result.addTag('customer', customer)
    result.addTag('utterance', utterance)
    result.addOutputField('Alexa Provider', alexaProvider)
    result.addOutputField('Transcript', _.get(result, 'lastResponse.transcript'))

    const expectedStream = record.expectedFields['streamURL']
    result.success = streamURL && streamURL.includes(expectedStream)
    
    // No need to verify silence if results has not been successful (i.e. streamURL didn't match)
    if (result.success) {
      let failureReason

      if (!streamURL) {
        failureReason = 'UNRECOGNIZED'
      }

      // Check for silence that exceeds thresholds
      if (this.checkStreams && !failureReason) {
        const data = await this._downloadStream(streamURL)
        // fs.writeFileSync('audio.aac', data)
        result.addOutputField('Stream Data Length', data ? data.length : 'NA')
        if (data) {
          const detectionResult = await this._silenceDetection(data)
          result.addOutputField('Silence Durations', detectionResult.durations || 'No Data - Error Processing Audio With FFMPEG')
          if (detectionResult.exceeded) {
            failureReason = 'SILENCE EXCEEDED'
          }
        } else {
          failureReason = 'DOES NOT PLAY'
        }
      }
      result.success = !failureReason

      // Add tags
      result.addTag('failureReason', failureReason)
      result.addOutputField('Failure Reason', failureReason)
    }

    return true
  }

  /**
   * Download data from the stream URL
   * @param {string} streamURL
   */
  async _downloadStream (streamURL) {
    // We use a promise because this operations needs to be able to be cancelled
    return new Promise((resolve, reject) => {
      const CancelToken = axios.CancelToken
      const source = CancelToken.source()

      // The audio duration is in seconds
      // The audio is encoded at 54 kilobits per second (54 * 1024)
      // There are 8 bits per byte ( divide by 8)
      const duration = Config.get('silenceDetection', undefined, true).streamDuration
      const bytesToRead = duration * 54 * 1024 / 8
      console.log(`AUDIO DOWNLOAD duration: ${duration} bytes: ${bytesToRead} url: ${streamURL}`)
      let audioData = Buffer.alloc(0)
      const startTime = Date.now()
      axios.get(streamURL, {
        cancelToken: source.token,
        responseType: 'stream'
      }).then((response) => {
        response.data.on('data', (data) => {
          audioData = Buffer.concat([audioData, data])
          if (audioData.length >= bytesToRead) {
            console.log('DOWNLOAD completed data: ' + audioData.length + ' lapsed-time: ' + (Date.now() - startTime) / 1000)
            source.cancel('Cancel stream GET')
            resolve(audioData)
          }
        })

        response.data.on('end', () => {
          console.info('DOWNLOAD end')
          
        })
      }).catch((e) => {
        if (axios.isCancel(thrown)) {
          console.info('DOWNLOAD cancel')
          resolve(audioData)
        } else {
          console.error('ERROR reading AUDIO data: ' + e.message)
          resolve(undefined)
        }
        
      })
    })
  }

  /**
   *
   * @param {Buffer} audioInput
   * @returns {SilenceResult}
   */
  async _silenceDetection (audioInput) {
    const ffmpegToken = Config.env('FFMPEG_TOKEN')
    const properties = Config.get('silenceDetection', undefined, true)
    const encodedAudio = audioInput.toString('base64')
    const jsonData = {
      command: `ffmpeg -i audio.mp3 -af silencedetect=n=${properties.level}dB:d=0.5 -f null -`,
      input: {
        'audio.mp3': encodedAudio
      }
    }

    try {
      console.info(`AUDIO SILENCE calling ffmpeg: ${jsonData.command}`)
      const response = await axios.post('https://ffmpeg.bespoken.io', jsonData, {
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': ffmpegToken
        },
        responseType: 'json'
      })

      // console.log('FFMPEG Output ' + JSON.stringify(response.data, null, 2))
      const silences = this._detectSilences(response.data.stderr)
      return {
        durations: silences,
        exceeded: silences.some(silence => silence > properties.silenceDuration)
      }
    } catch (e) {
      console.error(e.message)
      return {
        exceeded: false,
        error: e.message
      }
    }
  }

  /**
   * Searches through the file for all silence durations, then returns an array
   * @param {string} ffmpegOut
   */
  _detectSilences (ffmpegOut) {
    // Silence data looks like this:
    // [silencedetect @ 00000149fd86fdc0] silence_start: 0.243311
    // [silencedetect @ 00000149fd86fdc0] silence_end: 0.800816 | silence_duration: 0.557506
    const regex = /.*silence_duration: (.*)/g
    let matches
    const silences = []
    while (matches = regex.exec(ffmpegOut)) { // eslint-disable-line
      silences.push(parseFloat(parseFloat(matches[1]).toFixed(2)))
    }
    console.log('AUDIO SILENCE silences: ' + silences)
    return silences
  }
}

module.exports = StreamingAudioInterceptor
